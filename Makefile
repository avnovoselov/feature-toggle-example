up:
	docker-compose up -d

restart:
	docker-compose down && docker-compose up -d

install:
	cd symfony && composer install

tests:
	docker exec -ti Application bash -c "bin/phpunit tests/Unit"
