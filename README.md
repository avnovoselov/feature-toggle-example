# Feature flag dependent strategies

## Required

* PHP >7.4
* Docker
* Composer

Clone repository

```shell
git clone https://gitlab.com/avnovoselov/feature-toggle-example.git && cd feature-toggle-example
```

Install dependencies and start docker compose

```shell
make install && make up
```

HTTP service start at `localhost:8080`

Example URL

```
http://localhost:8080/greeting?id=<user.id>&name=<user.name>&birthday=<YYYY-MM-DD>
```

Example response

```json
{
  "greeting": "Welcome to hell Username",
  "billing": "Alfa",
  "greeting_bucket": 4,
  "greeting_class": "App\\Context\\Greeting\\FunExperiment\\SpookyStrategy",
  "billing_bucket": 1,
  "billing_class": "App\\Context\\Billing\\ExperimentalApi\\AlfaStrategy"
}
```

`Billing` and `greeting` class depends on `user.id`.

Greeting strategy `PrettyStrategy` and `SpookyStrategy` enabled to 10% users both.
Another users get `DefaultStrategy` greeting.

Users under 18 yo newer get `SpookyStrategy`.

`AlfaStrategy` available up to 5% users. 

## Configuration examples

All configuration can be stored in json docs (redis/mongo/file)

```yaml
# services.yaml priority configuration
App\Context\Billing\DefaultStrategy:
    arguments:
        $priority: 0
    App\Context\Billing\ExperimentalApi\AlfaStrategy:
        arguments:
            $priority: 10
    App\Context\Billing\ExperimentalApi\QiwiStrategy:
        arguments:
            $priority: 32

```

```yaml
# services.yaml distribution configuration
App\Context\Greeting\FunExperiment\PrettyStrategy:
    arguments:
        $buckets: [ 1 ] # only first of 10 group get this strategy
App\Context\Greeting\FunExperiment\SpookyStrategy:
    arguments:
        $buckets: [ 4 ] # only forth of 10 group get this strategy
```

```php
// AlfaStrategy.php distribution configuration
class AlfaStrategy implements StrategyInterface
{
    private const BUCKET_AMOUNT = 20;
    
    // another methods
    
    public function canProcess(UserDto $userDto): bool
    {
        return $this->getUserBucket($userDto->getIdentifier()) === 1; // distribution equals 5% (1/20)
    }

    // ...
```

```php
//SpookyStrategy.php extended/custom rules
class SpookyStrategy extends AbstractStrategy
{
    private const MIN_AVAILABLE_AGE = 18;

    // another methods

    public function canProcess(UserDto $userDto): bool
    {
        $now = new DateTimeImmutable();

        // disallow strategy to minor user
        if ($now->diff($userDto->getBirthday())->y < self::MIN_AVAILABLE_AGE) {
            return false;
        }

        return parent::canProcess($userDto);
    }

    // ...
```

