<?php

namespace App\Util;

use Savvot\Random\AbstractRand;
use Savvot\Random\RandException;
use Savvot\Random\XorShiftRand;

class PersistentRandomSequenceGenerator
{
    private AbstractRand $rand;

    public function __construct()
    {
        $this->rand = new XorShiftRand();
    }

    /**
     * Return random int <rnd> 0 < <rnd> < $buckets from random sequence based on "<$seed><$experiment>" string
     *
     * @param int $buckets
     * @param string $seed
     * @param string|null $experiment
     * @return int
     * @throws RandException
     */
    public function random(int $buckets, string $seed, string $experiment = null): int
    {
        $this->rand->setSeed($seed . ($experiment ?? ''));

        return $this->rand->random(0, $buckets - 1);
    }
}
