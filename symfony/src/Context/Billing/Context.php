<?php

namespace App\Context\Billing;

use App\Common\Dto\UserDto;
use App\Context\Billing\ExperimentalApi\AlfaStrategy;
use App\Context\Billing\ExperimentalApi\QiwiStrategy;
use Exception;

/**
 * Greeting strategy context
 * todo make common abstract template with simple configuration
 */
class Context
{
    /** @var StrategyInterface[] */
    private array $strategies = [];

    /**
     * To enable any billing strategy add them instance to constructor
     * and call addStrategy method
     *
     * @param DefaultStrategy $defaultStrategy
     * @param QiwiStrategy $qiwiStrategy
     * @param AlfaStrategy $alfaStrategy
     */
    public function __construct(
        DefaultStrategy $defaultStrategy,
        QiwiStrategy $qiwiStrategy,
        AlfaStrategy $alfaStrategy
    ) {
        $this->addStrategy($defaultStrategy);
        $this->addStrategy($qiwiStrategy);
        $this->addStrategy($alfaStrategy);

        $this->sortStrategies();
    }

    /**
     * Enable strategy
     *
     * @param StrategyInterface $strategy
     */
    public function addStrategy(StrategyInterface $strategy): void
    {
        $this->strategies[] = $strategy;
    }

    /**
     * Select first available strategy and call process method.
     * When context has no available strategy throw exception.
     *
     * @param UserDto $userDto
     * @return BillingDto
     * @throws Exception
     */
    public function handle(UserDto $userDto): BillingDto
    {
        foreach ($this->strategies as $strategy) {
            if ($strategy->canProcess($userDto)) {
                return $strategy->process($userDto);
            }
        }

        throw new Exception('I have no supported strategy');
    }

    /**
     * Sort enabled strategies by StrategyInterface::getPriority() value
     */
    private function sortStrategies(): void
    {
        usort(
            $this->strategies,
            fn(StrategyInterface $a, StrategyInterface $b) => $b->getPriority() <=> $a->getPriority()
        );
    }
}
