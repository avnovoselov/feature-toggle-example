<?php

namespace App\Context\Billing;

use App\Common\Dto\UserDto;

class DefaultStrategy implements StrategyInterface
{
    private int $priority;

    public function __construct(int $priority)
    {
        $this->priority = $priority;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function canProcess(UserDto $userDto): bool
    {
        return true;
    }

    public function process(UserDto $userDto): BillingDto
    {
        return new BillingDto('Tinkoff', ['class' => self::class]);
    }
}
