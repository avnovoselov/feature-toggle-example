<?php

namespace App\Context\Billing;

/**
 * Currency data container
 */
class BillingDto
{
    private string $api;

    private array $payload;

    public function __construct(string $api, array $payload = [])
    {
        $this->api     = $api;
        $this->payload = $payload;
    }

    public function getApi(): string
    {
        return $this->api;
    }

    public function getPayload(): array
    {
        return $this->payload;
    }
}
