<?php

namespace App\Context\Billing\ExperimentalApi;

use App\Common\Dto\UserDto;
use App\Context\Billing\BillingDto;
use App\Context\Billing\StrategyInterface;

/**
 * Dead strategy disabled
 * @deprecated
 */
class QiwiStrategy implements StrategyInterface
{
    private int $priority;

    public function __construct(int $priority)
    {
        $this->priority = $priority;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function canProcess(UserDto $userDto): bool
    {
        return false;
    }

    public function process(UserDto $userDto): BillingDto
    {
        return new BillingDto('Qiwi', ['class' => self::class]);
    }
}
