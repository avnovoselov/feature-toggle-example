<?php

namespace App\Context\Billing\ExperimentalApi;

use App\Common\Dto\UserDto;
use App\Context\Billing\BillingDto;
use App\Context\Billing\StrategyInterface;
use App\Util\PersistentRandomSequenceGenerator;
use Savvot\Random\RandException;

/**
 * New experimental billing implementation
 * enabled for 5% of users
 */
class AlfaStrategy implements StrategyInterface
{
    private const BUCKET_AMOUNT = 20;

    private int $priority;

    private PersistentRandomSequenceGenerator $prsg;

    public function __construct(int $priority, PersistentRandomSequenceGenerator $prsg)
    {
        $this->priority = $priority;
        $this->prsg     = $prsg;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param UserDto $userDto
     * @return bool
     * @throws RandException
     */
    public function canProcess(UserDto $userDto): bool
    {
        return $this->getUserBucket($userDto->getIdentifier()) === 1;
    }

    /**
     * @param UserDto $userDto
     * @return BillingDto
     * @throws RandException
     */
    public function process(UserDto $userDto): BillingDto
    {
        return new BillingDto('Alfa', [
            'class'  => self::class,
            'bucket' => $this->getUserBucket($userDto->getIdentifier()),
        ]);
    }

    /**
     * @param string $identifier
     * @return int
     * @throws RandException
     */
    private function getUserBucket(string $identifier): int
    {
        return $this->prsg->random(self::BUCKET_AMOUNT, $identifier, self::class);
    }
}
