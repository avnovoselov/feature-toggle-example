<?php

namespace App\Context\Greeting;

use App\Common\Dto\UserDto;
use App\Context\Greeting\FunExperiment\PrettyStrategy;
use App\Context\Greeting\FunExperiment\SpookyStrategy;
use Exception;

/**
 * Greeting strategy context
 * todo make common abstract template with simple configuration
 */
class Context
{
    /** @var StrategyInterface[] */
    private array $strategies = [];

    /**
     * To enable any greeting strategy add them instance to constructor
     * and call addStrategy method
     *
     * @param DefaultStrategy $defaultStrategy
     * @param PrettyStrategy $prettyStrategy
     * @param SpookyStrategy $spookyStrategy
     */
    public function __construct(
        DefaultStrategy $defaultStrategy,
        PrettyStrategy $prettyStrategy,
        SpookyStrategy $spookyStrategy
    ) {
        $this->addStrategy($defaultStrategy);
        $this->addStrategy($spookyStrategy);
        $this->addStrategy($prettyStrategy);

        $this->sortStrategies();
    }

    /**
     * Enable strategy
     *
     * @param StrategyInterface $strategy
     */
    public function addStrategy(StrategyInterface $strategy): void
    {
        $this->strategies[] = $strategy;
    }

    /**
     * Select first available strategy and call process method.
     * When context has no available strategy throw exception.
     *
     * @param UserDto $userDto
     * @return GreetingDto
     * @throws Exception
     */
    public function handle(UserDto $userDto): GreetingDto
    {
        foreach ($this->strategies as $strategy) {
            if ($strategy->canProcess($userDto)) {
                return $strategy->process($userDto);
            }
        }

        throw new Exception('I have no supported strategy');
    }

    /**
     * Sort enabled strategies by StrategyInterface::getPriority() value
     */
    private function sortStrategies(): void
    {
        usort(
            $this->strategies,
            fn(StrategyInterface $a, StrategyInterface $b) => $b->getPriority() <=> $a->getPriority()
        );
    }
}
