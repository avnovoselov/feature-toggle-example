<?php

namespace App\Context\Greeting\FunExperiment;

class PrettyStrategy extends AbstractStrategy
{
    /**
     * @inheritDoc
     */
    protected function getGreetings(): array
    {
        return [
            'Hi, %s ^^',
            'Hi, %s ))',
            'Meow, %s',
            '%s, we have missed you',
        ];
    }
}
