<?php

namespace App\Context\Greeting\FunExperiment;

use App\Common\Dto\UserDto;
use App\Context\Greeting\GreetingDto;
use App\Context\Greeting\StrategyInterface;
use App\Util\PersistentRandomSequenceGenerator;
use Savvot\Random\RandException;

/**
 * Abstract experiment strategy
 * Contain immutable priority for experiment
 */
abstract class AbstractStrategy implements StrategyInterface
{
    protected array $buckets;

    private const BUCKET_AMOUNT = 10;

    protected PersistentRandomSequenceGenerator $prsg;

    /**
     * Should return array of greeting templates
     * "%s" -> <username>,
     * ex: Hello %s! -> Hello Username!
     *
     * @return string[]
     */
    abstract protected function getGreetings(): array;

    public function __construct(array $buckets, PersistentRandomSequenceGenerator $prsg)
    {
        $this->buckets = $buckets;
        $this->prsg    = $prsg;
    }

    /**
     * Return constant to all experiment strategies
     *
     * @return int
     */
    final public function getPriority(): int
    {
        return 10;
    }

    /**
     * Generate persistent bucket for current user
     * Check is current strategy applicable to user bucket
     *
     * @param UserDto $userDto
     * @return bool
     * @throws RandException
     */
    public function canProcess(UserDto $userDto): bool
    {
        $bucket = $this->getUserBucket($userDto->getIdentifier());

        $availableBuckets = $this->getApplicableBuckets();

        return in_array($bucket, $availableBuckets);
    }

    /**
     * Get one random greeting and render string template with username
     *
     * @param UserDto $userDto
     * @return GreetingDto
     * @throws RandException
     */
    final public function process(UserDto $userDto): GreetingDto
    {
        $greetings = $this->getGreetings();
        $greeting  = $greetings[rand(0, count($greetings) - 1)];

        return new GreetingDto(
            sprintf($greeting, $userDto->getName()),
            [
                'bucket' => $this->getUserBucket($userDto->getIdentifier()),
                'class'  => static::class,
            ]
        );
    }

    /**
     * Dev/debug method
     *
     * @return int
     */
    final public static function getBucketAmount(): int
    {
        return self::BUCKET_AMOUNT;
    }

    /**
     * Return array of buckets pass through current strategy
     *
     * @return int[]
     */
    protected function getApplicableBuckets(): array
    {
        return $this->buckets;
    }

    /**
     * @param string $identifier
     * @return int
     * @throws RandException
     */
    final private function getUserBucket(string $identifier): int
    {
        return $this->prsg->random(self::BUCKET_AMOUNT, $identifier, self::class);
    }
}
