<?php

namespace App\Context\Greeting\FunExperiment;

use App\Common\Dto\UserDto;
use DateTimeImmutable;

/**
 * This strategy available 18+
 */
class SpookyStrategy extends AbstractStrategy
{
    private const MIN_AVAILABLE_AGE = 18;

    /**
     * @inheritDoc
     */
    protected function getGreetings(): array
    {
        return [
            'Boo! %s',
            'Are you scared %s?',
            'Welcome to hell %s',
        ];
    }

    /**
     * @inheritDoc
     */
    public function canProcess(UserDto $userDto): bool
    {
        $now = new DateTimeImmutable();

        // disallow strategy to minor user
        if ($now->diff($userDto->getBirthday())->y < self::MIN_AVAILABLE_AGE) {
            return false;
        }

        return parent::canProcess($userDto);
    }
}
