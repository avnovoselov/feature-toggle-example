<?php

namespace App\Context\Greeting;

use App\Common\Dto\UserDto;

/**
 * Base greeting strategy
 * "Hello <username>!"
 */
class DefaultStrategy implements StrategyInterface
{
    /**
     * Minimal priority
     *
     * @return int
     */
    public function getPriority(): int
    {
        return PHP_INT_MIN;
    }

    /**
     * Default strategy available any time
     *
     * @param UserDto $userDto
     * @return bool
     */
    public function canProcess(UserDto $userDto): bool
    {
        return true;
    }

    /**
     * Return "Hello <username>!"
     *
     * @param UserDto $userDto
     * @return GreetingDto
     */
    public function process(UserDto $userDto): GreetingDto
    {
        return new GreetingDto("Hello {$userDto->getName()}!", ['class' => self::class]);
    }
}
