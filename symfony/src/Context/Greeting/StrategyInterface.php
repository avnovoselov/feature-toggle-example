<?php

namespace App\Context\Greeting;

use App\Common\Dto\UserDto;

interface StrategyInterface
{
    /**
     * Return strategy priority less result == less priority
     *
     * @return int
     */
    public function getPriority(): int;

    /**
     * Should apply current algorithm?
     *
     * @param UserDto $userDto
     * @return bool
     */
    public function canProcess(UserDto $userDto): bool;

    /**
     * Return greeting string
     *
     * @param UserDto $userDto
     * @return GreetingDto
     */
    public function process(UserDto $userDto): GreetingDto;
}
