<?php

namespace App\Context\Greeting;

/**
 * Greeting data container
 */
final class GreetingDto
{
    private string $greeting;

    private array $payload;

    public function __construct(string $greeting, array $payload = [])
    {
        $this->greeting = $greeting;
        $this->payload  = $payload;
    }

    public function getGreeting(): string
    {
        return $this->greeting;
    }

    public function getPayload(): array
    {
        return $this->payload;
    }
}
