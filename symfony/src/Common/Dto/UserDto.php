<?php

namespace App\Common\Dto;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;

/**
 * Common User data container
 */
class UserDto
{
    private string $identifier;

    private string $name;

    private DateTimeInterface $birthday;

    /**
     * @throws Exception
     */
    public function __construct(string $identifier, string $name, string $birthday)
    {
        $this->identifier = $identifier;
        $this->name       = $name;
        $this->birthday   = new DateTimeImmutable($birthday);
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getBirthday(): DateTimeInterface
    {
        return $this->birthday;
    }
}
