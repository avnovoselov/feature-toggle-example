<?php

namespace App\Controller;

use App\Context\Billing\Context as BillingContext;
use App\Context\Greeting\Context as GreetingContext;
use App\Controller\EntryPoint\GreetingRequestDataMapper;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Demo entry point
 */
class EntryPointController extends AbstractController
{
    /**
     * @Route("/greeting", methods={"GET"})
     *
     * @param Request $request
     * @param GreetingRequestDataMapper $dataMapper
     * @param GreetingContext $greetingContext
     * @param BillingContext $billingContext
     * @return JsonResponse
     * @throws Exception
     */
    public function greeting(
        Request $request,
        GreetingRequestDataMapper $dataMapper,
        GreetingContext $greetingContext,
        BillingContext $billingContext
    ): JsonResponse {
        try {
            $userDto = $dataMapper->build($request->query);
        } catch (Exception $e) {
            return $this->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        $greeting = $greetingContext->handle($userDto);
        $billing  = $billingContext->handle($userDto);

        return $this->json([
            'greeting'        => $greeting->getGreeting(),
            'billing'         => $billing->getApi(),
            'greeting_bucket' => $greeting->getPayload()['bucket'] ?? '',
            'greeting_class'  => $greeting->getPayload()['class'] ?? '',
            'billing_bucket'  => $billing->getPayload()['bucket'] ?? '',
            'billing_class'   => $billing->getPayload()['class'] ?? '',
        ]);
    }
}
