<?php

namespace App\Controller\EntryPoint;

use App\Common\Dto\UserDto;
use Exception;
use Symfony\Component\HttpFoundation\InputBag;

class GreetingRequestDataMapper
{
    /**
     * @param InputBag $bag
     * @return UserDto
     * @throws Exception
     */
    public function build(InputBag $bag): UserDto
    {
        if ($bag->get('id') === null) {
            throw new Exception('Parameter "id" is required');
        }
        if ($bag->get('name') === null) {
            throw new Exception('Parameter "name" is required');
        }
        if ($bag->get('birthday') === null) {
            throw new Exception('Parameter "birthday" is required');
        }

        return new UserDto($bag->get('id'), $bag->get('name'), $bag->get('birthday'));
    }
}
