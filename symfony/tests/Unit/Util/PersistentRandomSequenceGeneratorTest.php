<?php

namespace App\Tests\Unit\Util;

use App\Util\PersistentRandomSequenceGenerator;
use Generator;
use LucidFrame\Console\ConsoleTable;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Savvot\Random\RandException;

class PersistentRandomSequenceGeneratorTest extends TestCase
{
    /**
     * @dataProvider sequenceDataProvider
     * @throws RandException
     */
    public function testRandom(string $id, string $experimentName, int $buckets, int $random)
    {
        $prsg   = new PersistentRandomSequenceGenerator();
        $result = $prsg->random($buckets, $id, $experimentName);

        $this->assertEquals($random, $result);
    }

    /**
     * @throws RandException
     */
    public function testDistribution()
    {
        $userAmount   = 500000;
        $bucketAmount = 10;
        $accuracy     = 7.5; // percent
        $distribution = array_fill(0, $bucketAmount, 0);

        $prsg  = new PersistentRandomSequenceGenerator();
        $table = new ConsoleTable();

        for ($i = 0; $i < $userAmount; $i++) {
            $fakeUuid = Uuid::uuid4();
            $bucket   = $prsg->random($bucketAmount, $fakeUuid->toString(), __METHOD__);

            $distribution[$bucket]++;
        }

        $table->setHeaders(['Bucket', 'Amount', 'Percentage %']);

        foreach ($distribution as $key => $value) {
            $table->addRow([$key, $value, round($value / $userAmount * 100, 2)]);

            $difference = abs(($userAmount / $bucketAmount - $value) / ($userAmount / $bucketAmount) * 100);
            $this->assertLessThanOrEqual($accuracy, $difference);
        }
        echo PHP_EOL;
        
        $table->display();
    }

    public function sequenceDataProvider(): Generator
    {
        yield '#1' => [
            'id'             => 'first fake uuid',
            'experimentName' => 'first',
            'buckets'        => 10,
            'random'         => 2,
        ];
        yield '#2' => [
            'id'             => 'second f@ke id',
            'experimentName' => 'second',
            'buckets'        => 20,
            'random'         => 11,
        ];
        yield '#3' => [
            'id'             => 'ThIrd unique identiFier',
            'experimentName' => 'third',
            'buckets'        => 5,
            'random'         => 0,
        ];
    }
}
